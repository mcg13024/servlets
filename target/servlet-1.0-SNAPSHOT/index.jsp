<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>

<h1><%= "Basic Form Servlet" %>

</h1>
<form action="BasicFormServlet" method="post">
    <p>Name<input name="Name" type="text" /></p>
    <p>Age<input name="Age" type="text" /></p>
    <p>Favorite Pie Flavor<input name="Pie" type="text"></p>
    <p>Hair Color<input name="Hair Color" type="text"></p>
    <p>I am not a Robot<input name="Proof of Humanity" type="checkbox"> </p>
    <p>I have read the following..bla bla bla...Legal Jargon... bla bla bla.<input name="Legal Jargon" type="checkbox"></p>
    <p>I have said "Hello" to the World. <input name="Greetings" type="checkbox"> </p>
    <p><input type="Submit" value="Submit" /></p>
</form>


<br/>
<a href="login.html">login page.</a>

</body>
</html>