package com.basic.forum;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

@WebServlet(name = "basicforum", value = "/basic-forum")
 class BasicforumServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileNotFoundException {



        String postText = request.getParameter( "postforum");
        System.out.println(postText);

        FileWriter writer = new FileWriter("forumposts.txt", true);
        BufferedWriter buff = new BufferedWriter(writer);
        buff.newLine();
        buff.write(postText);
        buff.close();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileNotFoundException {
        try {


            response.setContentType("text/html");


            File forum = new File("forumposts");
            if (forum.createNewFile()) {

            } else {
                System.out.println("File exists. Reading...");

            }
            Scanner fileinput = new Scanner(forum);

            PrintWriter out = response.getWriter();
            out.println("<html><body>");
            out.println("<h1>" + "Welcome to the forum" + "</h1>");

            out.println("<p><forum name-\"postforum\" method-\"post\" >" +
                    "<input type-\"text\" name-\"textfield\" value-\"\" maxlength-\"100\">" +
                    "</p><input type-\"Submit\" value-\"Post to forum\"></forum><br>");

            out.println("<h2>forum posts or below.</h2><br>");

            System.out.println("Scanning " + forum);


            ArrayList<String> posts = new ArrayList<String>();
            System.out.println("created posts List.");

            System.out.println("Within the while loop...");
            while (fileinput.hasNextLine()) {
                String post = fileinput.nextLine();
                posts.add(post);
            }


            System.out.println("Finished reading.");
            fileinput.close();
            System.out.println("Displaying posts List...");
            for (int i = 0; i < posts.size(); i++) {
                System.out.println("<p>" + posts.get(i) + "</p>");
            }
            System.out.println("</body></html>");

        } catch (FileNotFoundException e) {
            System.out.println("Error: Sorry, your file doesn't exist.");
            return;

        }
    }}



